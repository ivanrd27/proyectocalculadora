
package folder;

/**
 *
 * @author freyes
 */
public class Calcular2 {

    private static float Result;
    private static String Expre;
    private static String Expre2;
    private static int Sw;

    public static float getResult() {
        return Result;
    }

    public static void setResult(float aResult) {
        Result = aResult;
    }

    public static String getExpre() {
        return Expre;
    }

    public static void setExpre(String aExpre) {
        Expre = aExpre;
    }
    public static int getSw() {
        return Sw;
    }

    public static void setSw(int aSw) {
        Sw = aSw;
    }
     public static String getExpre2() {
        return Expre2;
    }
 
    public static void setExpre2(String aExpre2) {
        Expre2 = aExpre2;
    }

    public String Suma(String num) {
        Calcular2.setResult(Calcular2.getResult() + Float.parseFloat(num));
        
       Calcular2.setSw(1);
        if (esEntero(Calcular2.getResult())) {
            int var = Math.round(Calcular2.getResult());
            return Integer.toString(var);
        } else {
            return Float.toString(Calcular2.getResult()) ;
        }

    }
    public String Resta(String num) {
        Calcular2.setResult( Float.parseFloat(num)-Calcular2.getResult());
        
        
       Calcular2.setSw(1);
        if (esEntero(Calcular2.getResult())) {
            int var = Math.round(Calcular2.getResult());
            return Integer.toString(var);
        } else {
            return Float.toString(Calcular2.getResult()) ;
        }

    }
    public String Multiplicar(String num) {
        if (Calcular2.getResult() > 0) {
            Calcular2.setResult(Calcular2.getResult() * Float.parseFloat(num));
        } else {
            Calcular2.setResult(Float.parseFloat(num));
        }

        Calcular2.setSw(1);
        if (esEntero(Calcular2.getResult())) {
            int var = Math.round(Calcular2.getResult());
            return Integer.toString(var);
        } else {
            return Float.toString(Calcular2.getResult());
        }

    }

    public String Dividir(String num) {
        if (Calcular2.getResult() > 0) {
            Calcular2.setResult(Float.parseFloat(num) / Calcular2.getResult());
        } else {
            Calcular2.setResult(Float.parseFloat(num));
        }

        Calcular2.setSw(1);
        if (esEntero(Calcular2.getResult())) {
            int var = Math.round(Calcular2.getResult());
            return Integer.toString(var);
        } else {
            return Float.toString(Calcular2.getResult());
        }

    }

    public String Porcentaje(String num) {
        float por = 0;
        //float por =(float)0.2;
        if (Calcular2.getResult() > 0) {
            por = ((Float.parseFloat(num) * Calcular2.getResult()) / 100);
            if (getExpre() == "+") {
                Calcular2.setResult(Calcular2.getResult() + por);
            }
            if (getExpre() == "-") {
                Calcular2.setResult(Calcular2.getResult() - por);
            }
            if (getExpre() == "*" || getExpre() == "/") {
                por = (Float.parseFloat(num) / 100);
                //Calcular2.setResult(Calcular2.getResult() * por);
               
                Calcular2.setSw(1);
                if (esEntero(por)) {
                    int var = Math.round(por);
                    return Integer.toString(var);
                } else {
                    return Float.toString(por);
                }
            }
            /*if (getExpre() == "/") {
                Calcular2.setResult(Calcular2.getResult() / por);
               
            }
             */

            //=(6200*50)/100
        } else {
            Calcular2.setResult(Float.parseFloat(num));
        }
        //Calcular2.setResult(por);

        Calcular2.setSw(1);
        if (esEntero(Calcular2.getResult())) {
            int var = Math.round(Calcular2.getResult());
            return Integer.toString(var);
        } else {
            return Float.toString(Calcular2.getResult());
        }

    }
    

    public String Igual(String num) {
        if (getExpre() == "+") {
            Calcular2.setResult(Calcular2.getResult() + Float.parseFloat(num));
        }
        if (getExpre() == "-") {
            Calcular2.setResult(Calcular2.getResult() - Float.parseFloat(num));
        }
        if (getExpre() == "*") {
            Calcular2.setResult(Calcular2.getResult() * Float.parseFloat(num));
        }
        if (getExpre() == "/") {
            Calcular2.setResult(Calcular2.getResult() / Float.parseFloat(num));
            System.out.println(Float.toString(Calcular2.getResult()) + ", " + num);
        }
        if(getExpre() == "³√")
        {
           return RaizEnesima(num);
        }

        Calcular2.setSw(1);
        float valor = Calcular2.getResult();
        Calcular2.setResult(0);
        if (esEntero(valor)) {
            int var = Math.round(valor);
            return Integer.toString(var);
        } else {
            return Float.toString(valor) ;
        }

    }

    public String Raiz(String num) {

        Calcular2.setResult((float) Math.sqrt(Float.parseFloat(num)));
        Calcular2.setSw(1);
        float valor = Calcular2.getResult();
        Calcular2.setResult(0);
        if (esEntero(valor)) {
            int var = Math.round(valor);
            return Integer.toString(var);
        } else {
            return Float.toString(valor) ;
        }
      
    }
    
     
    
    
     public String alCuadrado(String num) {

        Calcular2.setResult(Float.parseFloat(num)*Float.parseFloat(num));
        Calcular2.setSw(1);
        float valor = Calcular2.getResult();
        Calcular2.setResult(0);
        if (esEntero(valor)) {
            int var = Math.round(valor);
            return Integer.toString(var);
        } else {
            return Float.toString(valor) ;
        }
      
    }
     
    public String Logaritmo(String num) {
        double numero = Math.log10(Float.parseFloat(num));
        Calcular2.setResult((float)numero);
        Calcular2.setSw(1);
        float valor = Calcular2.getResult();
        Calcular2.setResult(0);
        if (esEntero(valor)) {
            int var = Math.round(valor);
            return Integer.toString(var);
        } else {
            return Float.toString(valor) ;
        }
      
    }
    
    public String RaizEnesima(String num) {
        if (Calcular2.getResult() > 0) {
            Calcular2.setResult((float) Math.pow(Calcular2.getResult(), (1 / Float.parseFloat(num))));
            Calcular2.setSw(1);
            if (esEntero(Calcular2.getResult())) {
                int var = Math.round(Calcular2.getResult());
                return Integer.toString(var);
            } else {
                return Float.toString(Calcular2.getResult());
            }
        } else {
            Calcular2.setResult(Float.parseFloat(num));
        }

        Calcular2.setSw(1);
        if (esEntero(Calcular2.getResult())) {
            int var = Math.round(Calcular2.getResult());
            return Integer.toString(var);
        } else {
            return Float.toString(Calcular2.getResult());
        }

    }
    
    public String fraccion(String num) {
        int denom;
        float d = Float.parseFloat(num);
        String s = num;
        int digitsDec = s.length() - 1 - s.indexOf('.');
        denom = 1;
        for (int i = 0; i < digitsDec; i++) {
            d *= 10;
            denom *= 10;
        }
        int numero = (int) Math.round(d);
        return Integer.toString(numero) + "/" + Integer.toString(denom);
    }
   
     
      public String alCubo(String num) {

        Calcular2.setResult(Float.parseFloat(num)*Float.parseFloat(num)*Float.parseFloat(num));
        Calcular2.setSw(1);
        float valor = Calcular2.getResult();
        Calcular2.setResult(0);
        if (esEntero(valor)) {
            int var = Math.round(valor);
            return Integer.toString(var);
        } else {
            return Float.toString(valor) ;
        }
      
    }
        public String valorPi(String num) {
        if ("+".equals(getExpre())) {
            Calcular2.setResult(Calcular2.getResult() +(float)3.14159265358979323);
        }
        if ("-".equals(getExpre())) {
            Calcular2.setResult(Calcular2.getResult() - (float)3.14159265358979323);
        }
        if ("*".equals(getExpre())) {
            Calcular2.setResult(Calcular2.getResult() * (float)3.14159265358979323);
        }
        if ("/".equals(getExpre())) {
            Calcular2.setResult(Calcular2.getResult() / (float)3.14159265358979323);
            
        }
        
        Calcular2.setSw(1);
        float valor = Calcular2.getResult();
        Calcular2.setResult(0);
        if (esEntero(valor)) {
            int var = Math.round(valor);
            return Integer.toString(var);
        } else {
            return Float.toString(valor) ;
        }
      
    }
        
    public String Euler(String num) {
        double valor = Double.parseDouble(num);
        Calcular2.setResult((float) Math.exp(valor));
        Calcular2.setSw(1);
        if (esEntero(Calcular2.getResult())) {
            int var = Math.round(Calcular2.getResult());
            return Integer.toString(var);
        } else {
            return Float.toString(Calcular2.getResult());
        }

    }
    
    public boolean esEntero(float numero) {
      
        if (numero % 1 == 0) {

            return true;

        } else {

            return false;

        }
        
    }

    
   

  
}
